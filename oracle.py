#!/usr/bin/env python3

import argparse, collections, itertools

DICT = "/usr/share/dict/words"


def alphaplus(s):
    return s.isalpha() or s == ""


def alphaplus_error(metavar):
    return ValueError(f"{metavar} must be alpha or empty")


def cli():
    yellows_metavar = "Y"
    greens_metavar = "G"
    blacks_metavar = "B"
    length = 5

    parser = argparse.ArgumentParser(description="Wordle solver")
    parser.add_argument(
        "-y",
        "--yellows",
        metavar=yellows_metavar,
        nargs=length,
        type=str,
        default=[""] * length,
        help="Yellow letters, eg '' t t er re",
    )
    parser.add_argument(
        "-g",
        "--greens",
        metavar=greens_metavar,
        type=str,
        default=" " * length,
        help='Green letters, eg " e  t"',
    )
    parser.add_argument(
        "-b",
        "--blacks",
        metavar=blacks_metavar,
        type=str,
        default="",
        help="Discounted letters, eg laso",
    )
    parser.add_argument(
        "-d",
        "--dedup",
        action="store_true",
        help="Don't consider words with repeated letters",
    )
    parser.add_argument(
        "-a",
        "--algorithm",
        choices=["freq", "pos"],
        default="freq",
        help="Choose search algorithm, default freq",
    )
    parser.add_argument(
        "-w",
        "--words",
        choices=["all", "sol", "linux", "og", "og_sol"],
        default="all",
        help=(
            "Choose dictionary to search, default all; "
            "options: all = NYT Wordle words, sol = NYT Wordle solutions, "
            "linux = local Linux dictionary, og = original Wordle words, "
            "og_sol = original Wordle solutions"
        ),
    )
    parser.add_argument(
        "-n",
        "--number",
        metavar="N",
        type=int,
        default=5,
        help="Maximal number of guesses to make, default 5",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Print debug statements",
    )
    args = parser.parse_args()

    if not all(map(alphaplus, args.yellows)):
        raise alphaplus_error(yellows_metavar)

    if not len(args.greens) == length:
        raise Exception(f"{greens_metavar} must be length {length}")

    if not alphaplus(args.blacks):
        raise alphaplus_error(blacks_metavar)

    args.yellows = [y.lower() for y in args.yellows]
    args.greens = args.greens.lower()
    args.blacks = args.blacks.lower()

    return args


def read_words(filename):
    with open(filename) as f:
        return f.read().strip().split("\n")


def order(dic):
    return sorted(dic.items(), key=lambda p: p[1], reverse=True)


def checker(word, dedup, green, yellow, ys, black):
    return (
        (len(set(word)) == 5 if dedup else True)
        and all(letter == g for letter, g in zip(word, green) if g.isalpha())
        and all(letter not in yy for letter, yy in zip(word, yellow))
        and all(y in word for y in ys)
        and all(b not in word for b in black if b not in green)
        and all(
            letter not in black for letter, g in zip(word, green) if not g.isalpha()
        )
    )


def search_pos(sols, yellow, black, green, dedup, n):
    # by letter positions

    ys = set("".join(yellow))

    matches = []
    for word in sols:
        if checker(word, dedup, green, yellow, ys, black):
            matches.append(word)

    pos_freqs = [collections.Counter() for _ in range(5)]

    for word in matches:
        for pos_freq, letter in zip(pos_freqs, word):
            pos_freq[letter] += 1

    if args.verbose:
        print("letter frequencies by position")
        for i, pos_freq in enumerate(pos_freqs):
            print(f"  {i} {order(pos_freq)[:5]}")

    pos_words = collections.Counter()
    for word in matches:
        for pos_freq, let in zip(pos_freqs, word):
            pos_words[word] += pos_freq[let]

    ordered = order(pos_words)[:n]

    if args.verbose:
        print(f"start pos_words\n  {ordered}")
    else:
        for word, _ in ordered:
            print(word)


def search_freq(sols, yellow, black, green, dedup, n):
    # by letter frequencies

    ys = set("".join(yellow))

    matches = []
    for word in sols:
        if checker(word, dedup, green, yellow, ys, black):
            matches.append(word)

    let_freq = collections.Counter()
    for word in matches:
        for letter in word:
            let_freq[letter] += 1

    if args.verbose:
        print(f"most frequent letters\n  {order(let_freq)[:5]}")

    freq_words = collections.Counter()
    for word in matches:
        for let in word:
            freq_words[word] += let_freq[let]

    ordered = order(freq_words)[:n]

    if args.verbose:
        print(f"freq_words\n  {ordered}")
    else:
        for word, _ in ordered:
            print(word)


def is_word(word):
    return word.isalpha() and word.islower()


def get_words(n=5, words=DICT):
    return [w for w in read_words(words) if len(w) == n and is_word(w)]


if __name__ == "__main__":
    args = cli()

    if args.words in ("all", "sol"):
        words = read_words("sol_words_nyt.txt")
        if args.words == "all":
            words += read_words("oth_words_nyt.txt")
    elif args.words in ("og", "og_sol"):
        words = read_words("sol_words.txt")
        if args.words == "og":
            words += read_words("oth_words.txt")
    elif args.words == "linux":
        words = get_words()
    if args.verbose:
        print(f"# words: {len(words)}")

    if args.algorithm == "freq":
        search_freq(
            words, args.yellows, args.blacks, args.greens, args.dedup, args.number
        )
    elif args.algorithm == "pos":
        search_pos(
            words, args.yellows, args.blacks, args.greens, args.dedup, args.number
        )
