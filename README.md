# [wordle-fun](https://gitlab.com/eidoom/wordle-fun)

* Wordle scripts
* Words taken from Wordle's JavaScript
    * `(sol|oth)_words.txt` from [original Wordle](https://www.powerlanguage.co.uk/wordle/) on 2022-01-29
    * `(sol|oth)_words_nyt.txt` from [NYT Wordle](https://www.nytimes.com/games/wordle/index.html) on 2022-04-06
* For usage help,
    * Use `python3 oracle.py --help` for help using the guesser
    * Use `python3 play.py --help` for help using the player
