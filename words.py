#!/usr/bin/env python3

import oracle

import matplotlib.pyplot

if __name__ == "__main__":
    all_words = [w for w in oracle.read_words(oracle.DICT) if oracle.is_word(w)]

    i = 1
    xs = []
    ys = []
    while True:
        n = sum(1 for w in all_words if len(w) == i)
        if not n:
            break
        i += 1
        xs.append(i)
        ys.append(n)

    matplotlib.pyplot.bar(xs, ys)

    matplotlib.pyplot.show()
