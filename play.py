#!/usr/bin/env python3

import argparse, datetime, collections, string

import oracle

UNICODE = {
    "white": "⬛",
    "green": "🟩",
    "yellow": "🟨",
    # "red": "🟥",
}

ESC = "\033"


# https://en.wikipedia.org/wiki/ANSI_escape_code#3-bit_and_4-bit
def colours(base):
    c = ("white", "red", "green", "yellow", "blue", "magenta", "cyan", "black")
    return {char: i for char, i in zip(c, range(base, base + len(c)))}


FG = colours(30)
BG = colours(40)


# https://en.wikipedia.org/wiki/ANSI_escape_code#CSI_(Control_Sequence_Introducer)_sequences
def move(d, n=""):
    c = {
        # "up": "A",
        # "down": "B",
        # "right": "C",
        # "left": "D",
        "next": "E",
        "prev": "F",
    }[d]
    return f"{ESC}[{n}{c}"


RESET = f"{ESC}[2K{ESC}[0G"


def coloured(content, bg):
    fg = "white" if bg == "black" else "black"
    return f"{ESC}[{FG[fg]};{BG[bg]}m{content}{ESC}[0m"


def attempt(guess, sol, freq):
    state = ["white"] * len(sol)
    doubles = collections.Counter()

    for i, (gl, sl) in enumerate(zip(guess, sol)):
        if gl == sl:
            doubles[gl] += 1

    for i, (gl, sl) in enumerate(zip(guess, sol)):
        if gl == sl:
            state[i] = "green"
        elif gl in sol and doubles[gl] < freq[gl]:
            doubles[gl] += 1
            state[i] = "yellow"

    return state


def result(guess, state):
    return move("prev") + "".join(coloured(g, s) for g, s in zip(guess, state))


def pretty(state):
    return "".join(UNICODE[a] for a in state)


def valid(guess, prev, state):
    for y, s in zip(prev, state):
        if s == "yellow":
            if y not in guess:
                return False
    for g, p, s in zip(guess, prev, state):
        if s == "green" and g != p:
            return False
    return True


def cli():
    parser = argparse.ArgumentParser(description="Play Wordle in the terminal!")
    parser.add_argument(
        "-*",
        "--hardmode",
        action="store_true",
        help="Enable hardmode",
    )
    parser.add_argument(
        "-g",
        "--guesses",
        metavar="G",
        type=int,
        default=6,
        help="Choose number of allowed guesses",
    )
    parser.add_argument(
        "-w",
        "--words",
        choices=["nyt", "og"],
        default="nyt",
        help="Choose dictionary [default nyt] {ignored if --custom set}",
    )
    exlusive = parser.add_mutually_exclusive_group()
    exlusive.add_argument(
        "-c",
        "--custom",
        metavar="C",
        type=str,
        help="Choose custom word by name of text file containing only it",
    )
    exlusive.add_argument(
        "-q",
        "--quick",
        metavar="Q",
        type=str,
        help="Set custom word",
    )
    exlusive.add_argument(
        "-d",
        "--date",
        metavar="YYYY-MM-DD",
        type=str,
        help="Choose a different word by date in ISO format",
    )
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = cli()

    print(f"Wordl{'*' if args.hardmode else ''}\n{args.guesses} guesses")

    filename = f"_words{'_nyt' if args.words == 'nyt' else ''}.txt"

    today = datetime.date.today()

    if args.custom or args.quick is not None:
        if args.custom is not None:
            with open(args.custom) as f:
                sol = f.read().strip()
        else:
            sol = args.quick
        sol = sol.lower()

        words = oracle.get_words(len(sol))

        if sol not in words:
            words.append(sol)

        number = "i"

    else:
        words = oracle.read_words(f"sol{filename}")

        date = (
            datetime.date.fromisoformat(args.date) if args.date is not None else today
        )
        if date > today:
            if input("Are you sure you want to venture into the future? [y] ") != "y":
                exit()
        start = datetime.date.fromisoformat("2021-06-19")
        number = (date - start).days % len(words)

        sol = words[number]

        words += oracle.read_words(f"oth{filename}")

    print(f"{len(sol)} letters\n")

    freq = collections.Counter()
    for letter in sol:
        freq[letter] += 1

    let_freq = collections.Counter()
    for word in words:
        for letter in word:
            let_freq[letter] += 1

    alphabet = [a for a, b in oracle.order(let_freq)]

    trans = {l: coloured(l, "black") for l in string.ascii_lowercase}
    print("".join(trans[l] for l in alphabet), "\n")

    states = []
    prev = None

    i = 1
    while True:
        guess = input().lower()

        if (len(guess) != len(sol) or guess not in words) or (
            args.hardmode and (prev and not valid(guess, prev, states[-1]))
        ):
            print(move("prev"), RESET, end="")
            continue

        prev = guess

        state = attempt(guess, sol, freq)

        print(result(guess, state))

        states.append(state)

        for g, s in zip(guess, state):
            trans[g] = coloured(g, s)

        n = i + 2
        print(
            move("prev", n) + "".join(trans[l] for l in alphabet) + move("next", n),
            end="",
        )

        if state == ["green"] * len(sol):
            break
        elif i == args.guesses:
            i = "X"
            print(sol.upper())
            break

        i += 1

    result = f"\nWordl {number} {i}/{args.guesses}{'*' if args.hardmode else ''}\n"
    for j, state in enumerate(states, start=1):
        result += pretty(state)
        if j != len(states):
            result += "\n"
    print(result)

    if args.custom or args.quick is not None:
        with open("result_custom", "a") as f:
            f.write(f"{sol}{result}\n\n")
    else:
        with open("result", "a") as f:
            f.write(f"{today.isoformat()}{result}\n\n")
